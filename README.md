# README #

Simple Single Truck and Trailer map editor. The current version supports a single depot, a single truck, a set of satellite depots and a set of truck customers.
Valid input (output) files are formatted as follows:

- The first line indicates the size of the problem: number of customers (n) and number of depots (p+1);
- The second line has the capacities of the truck and the trailer;
- p+1 lines with the x and y coordinates of the depots (the first line is for the main depot and the next correspond to the satellite depots); 
- n lines with the x and y coordinates of the customers and their demands.
