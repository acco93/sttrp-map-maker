package algorithms;

import java.util.List;
import java.util.Set;

import model.Vertex;

/**
 * 
 * Solution as a main tour and a set of subtours.
 * 
 * @author acco
 *
 */
public class Solution {

	Set<List<Vertex>> routes;
	float value;

	public Solution(Set<List<Vertex>> routes, float value) {
		this.routes = routes;
		this.value = value;
	}

	public Set<List<Vertex>> getRoutes() {
		return routes;
	}

	public float getValue() {
		return value;
	}

}
