package controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import algorithms.Solution;
import io.ioManager;
import model.IModel;
import model.Instance;
import model.Model;
import view.IView;
import view.View;

/**
 * Controller implementation in the MVP pattern.
 * 
 * @author acco
 *
 */
public class Controller implements IController {

	private IModel model;
	private IView view;
	private String currentPath;

	public Controller() {
		
		currentPath = null;
		model = new Model();
		view = new View(this);

	}

	@Override
	public void store(String path) {
		ioManager.writeToFile(path, model.getInstance());
		currentPath = path;
	}

	@Override
	public void load(String path) {
		this.model.resetInstance();
		ioManager.readFromFile(path, model.getInstance());
		// load the optimal solution if available
		//this.importSolutionFromFile(path+".sol", true);
		this.view.refresh();
		currentPath = path;
	}
	
	@Override
	public void loadMDCVRP(String path) {
		this.model.resetInstance();
		ioManager.readFromFileMDCVRP(path, model.getInstance());
		// load the optimal solution if available
		//this.importSolutionFromFile(path+".sol", true);
		this.view.refresh();
		currentPath = path;
	}

	@Override
	public boolean isFeasible() {

		Instance instance = this.model.getInstance();

		if (instance.getDepot() == null) {
			this.view.setError("Depot is missing");
			return false;
		}

		if(instance.getSatellites().size() + instance.getVehicleCustomersYesPark().size() == 0) {
			this.view.setError("At least one parking location is needed");
			return false;
		}
		
		int truckCustomersLoad = instance.getTruckCustomers().stream().map(vertex -> vertex.getLoad()).reduce(0,
				(x, y) -> x + y);

		int vehicleCustomersLoad = instance.getVehicleCustomersNoPark().stream().map(vertex -> vertex.getLoad()).reduce(0,
				(x, y) -> x + y);

		if (truckCustomersLoad + vehicleCustomersLoad > instance.getTruckCapacity() + instance.getTrailerCapacity()) {
			this.view.setError("Total customers demand is greater that vehicle capacity");
			return false;
		}

		return true;
	}

	@Override
	public int getTruckCapacity() {
		return this.model.getInstance().getTruckCapacity();
	}

	@Override
	public void setTruckCapacity(int value) {
		if (value < 1) {
			return;
		}
		this.model.getInstance().setTruckCapacity(value);
		this.view.refresh();
	}

	@Override
	public int getTrailerCapacity() {
		return this.model.getInstance().getTrailerCapacity();
	}

	@Override
	public void setTrailerCapacity(int value) {
		if (value < 1) {
			return;
		}
		this.model.getInstance().setTrailerCapacity(value);
		this.view.refresh();

	}

	@Override
	public Instance getInstance() {
		return this.model.getInstance();
	}

	@Override
	public void resetInstance() {
		this.model.resetInstance();
		this.view.refresh();
		currentPath = null;
	}

	@Override
	public void exit() {
		/*
		 * Check if something has to be saved
		 */
		System.exit(0);

	}


	@Override
	public void generate(int xMax, int yMax, int satellitesNum, int truckCustomersNum, int vehicleCustomersNum, int vehicleCustomersParkingFacilityNum,boolean type) {
		this.model.resetInstance();
		InstanceGenerator.generate(this.model.getInstance(), xMax, yMax, satellitesNum, truckCustomersNum,
				vehicleCustomersNum, vehicleCustomersParkingFacilityNum, type);
		this.view.refresh();
		currentPath = null;
	}

	@Override
	public void importSolutionFromFile(String path, boolean isOptimal) {

		try {
			InputStream stream = new FileInputStream(path);
			this.importSolution(stream, isOptimal);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void importSolutionFromStdin(String text, boolean isOptimal) {

		InputStream stream = new ByteArrayInputStream(text.getBytes(StandardCharsets.UTF_8));
		this.importSolution(stream, isOptimal);

	}

	
	private void importSolution(InputStream stream, boolean isOptimal) {

		Solution solution = ioManager.readSolution(stream, this.model.getInstance());
		if (solution != null) {
			if(isOptimal) {
				view.setOptimalSolution(solution);				
			} else {
				view.setSolution(solution);
			}

			view.setInfo("UB = " + solution.getValue());
		} else {
			view.setError("Solution - instance mismatch");
		}

	}

	@Override
	public boolean isCvrpFeasible() {
		return true;
	}

	@Override
	public void storeAsCvrp(String path) {
		ioManager.writeToFileAsCvrp(path, model.getInstance());
		currentPath = path;
		
	}

	@Override
	public void clearSolution(boolean isOptimal) {
		view.clearSolution(isOptimal);
		
	}

	@Override
	public void convert(File path) {
		ioManager.convertInstancesFromDirectory(path);
		
	}

	@Override
	public void overwrite() {
		
		if(currentPath == null) {
			view.setError("Nothing to overwrite!");
		} else {
			ioManager.writeToFile(currentPath, model.getInstance());
		}
		
	}

	@Override
	public void convertChao(File path) {
		ioManager.convertChaoInstancesToXSTTRPOnesFromDirectory(path);
	}




}
