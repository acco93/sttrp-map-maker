package controller;

import java.io.File;

import model.Instance;

/**
 * Controller interface.
 * 
 * @author acco
 *
 */
public interface IController {

	/**
	 * Exports the current instance into a file in the given path.
	 * 
	 * @param path
	 *            file path
	 */
	void store(String path);

	/**
	 * Import a file instance.
	 * 
	 * @param path
	 *            file path
	 */
	void load(String path);

	/**
	 * Returns true if the instance is feasible, false otherwise.
	 * 
	 * @return
	 */
	boolean isFeasible();

	/**
	 * Returns the truck capacity.
	 * 
	 * @return
	 */
	int getTruckCapacity();

	/**
	 * Updates the truck capacity.
	 * 
	 * @param value
	 */
	void setTruckCapacity(int value);

	/**
	 * Returns the trailer capacity.
	 * 
	 * @return
	 */
	int getTrailerCapacity();

	/**
	 * Updates the trailer capacity.
	 * 
	 * @param value
	 */
	void setTrailerCapacity(int value);

	/**
	 * Returns the current instance.
	 * 
	 * @return
	 */
	Instance getInstance();

	/**
	 * Create a new empty instance.
	 */
	void resetInstance();

	/**
	 * Close the application.
	 */
	void exit();


	void generate(int xMax, int yMax, int satellitesNum, int truckCustomersNum, int vehicleCustomersNum, int vehicleCustomersParkingFacilityNum, boolean type);

	void importSolutionFromFile(String path, boolean isOptimal);

	void importSolutionFromStdin(String text, boolean isOptimal);

	boolean isCvrpFeasible();

	void storeAsCvrp(String path);

	void clearSolution(boolean isOptimal);

	void convert(File path);

	void loadMDCVRP(String path);

	void overwrite();

	void convertChao(File path);


	
}
