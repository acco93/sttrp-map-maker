package controller;

import java.util.concurrent.ThreadLocalRandom;

import model.Instance;
import model.VertexType;

public class InstanceGenerator {

	public static void generate(Instance instance, int xMax, int yMax, int satellitesNum, int truckCustomersNum,

			int vehicleCustomersNum, int vehicleCustomersParkingFacilityNum, boolean type) {

		if(type) {
			generateRandom(instance, xMax, yMax, satellitesNum, truckCustomersNum, vehicleCustomersNum, vehicleCustomersParkingFacilityNum);

		} else {
			generateClustered(instance, xMax, yMax, satellitesNum, truckCustomersNum, vehicleCustomersNum, vehicleCustomersParkingFacilityNum);			
		}


	}

	private static void generateClustered(Instance instance, int xMax, int yMax, int satellitesNum, int truckCustomersNum,
			int vehicleCustomersNoParkNum, int vehicleCustomersYesParkNum) {
		
		/**
		 * Place the depot.
		 */
		int x = ThreadLocalRandom.current().nextInt(0, xMax + 1);
		int y = ThreadLocalRandom.current().nextInt(0, yMax + 1);
		instance.addVertex(x, y, VertexType.DEPOT, 0);
		instance.addVertex(x, y, VertexType.SATELLITE, 0);
		
		int num = satellitesNum + truckCustomersNum + vehicleCustomersNoParkNum + vehicleCustomersYesParkNum;
		
		int clusters = ThreadLocalRandom.current().nextInt(2, num / 10);
		
		System.out.println("There are " + clusters + " clusters");
		
		int squareWidth = (int) (xMax / clusters - 0.1 * (xMax / clusters));
		int squareHeight = (int) (yMax / clusters - 0.1 * (yMax / clusters));
		
		int satellitesPerSquare = satellitesNum / clusters;
		int truckCustomersPerSquare = truckCustomersNum / clusters;
		int vehicleCustomersNoParkPerSquare = vehicleCustomersNoParkNum / clusters;
		int vehicleCustomersYesParkPerSquare = vehicleCustomersYesParkNum / clusters;
		
		
		for(int c = 0; c < clusters; c++) {
		
			int xCentroid = ThreadLocalRandom.current().nextInt(squareWidth, xMax-squareWidth);
			int yCentroid = ThreadLocalRandom.current().nextInt(squareHeight, yMax-squareHeight);
			
			int xSquareMin = xCentroid - squareWidth / 2;
			int xSquareMax = 1 + xCentroid + squareWidth / 2;
			
			int ySquareMin = yCentroid - squareHeight / 2;
			int ySquareMax = 1 + yCentroid + squareHeight / 2;
			
			for (int i = 0; i < satellitesPerSquare; i++) {

				x = ThreadLocalRandom.current().nextInt(xSquareMin, xSquareMax);
				y = ThreadLocalRandom.current().nextInt(ySquareMin, ySquareMax);

				instance.addVertex(x, y, VertexType.SATELLITE, 0);
			}

			
			for (int i = 0; i < truckCustomersPerSquare; i++) {


				x = ThreadLocalRandom.current().nextInt(xSquareMin, xSquareMax);
				y = ThreadLocalRandom.current().nextInt(ySquareMin, ySquareMax);

				int load = ThreadLocalRandom.current().nextInt(1, 50);
				
				instance.addVertex(x, y, VertexType.TRUCK_CUSTOMER, load);
			}

			
			for (int i = 0; i < vehicleCustomersNoParkPerSquare; i++) {


				x = ThreadLocalRandom.current().nextInt(xSquareMin, xSquareMax);
				y = ThreadLocalRandom.current().nextInt(ySquareMin, ySquareMax);

				int load = ThreadLocalRandom.current().nextInt(1, 50);
				
				instance.addVertex(x, y, VertexType.VEHICLE_CUSTOMER_NO_PARK, load);
			}

			
			for (int i = 0; i < vehicleCustomersYesParkPerSquare; i++) {


				x = ThreadLocalRandom.current().nextInt(xSquareMin, xSquareMax);
				y = ThreadLocalRandom.current().nextInt(ySquareMin, ySquareMax);

				int load = ThreadLocalRandom.current().nextInt(1, 50);
				
				instance.addVertex(x, y, VertexType.VEHICLE_CUSTOMER_YES_PARK, load);
			}
			
			
		}
		
		int satellitesOutliers = 0;
		if(satellitesNum > 0) {
			satellitesOutliers = satellitesNum % satellitesPerSquare;
		}
		
		
		int truckCustomersOutliers = 0;
		if(truckCustomersNum > 0) {
			truckCustomersOutliers = truckCustomersNum % truckCustomersPerSquare;
		}
		
	
		int vehicleCustomersNoParkOutliers = 0;
		if(vehicleCustomersNoParkNum > 0) {
			vehicleCustomersNoParkOutliers = vehicleCustomersNoParkNum % vehicleCustomersNoParkPerSquare;
		}
		
		int vehicleCustomersYesParkOutliers = 0;
		if(vehicleCustomersNoParkNum > 0) {
			vehicleCustomersYesParkOutliers = vehicleCustomersYesParkNum % vehicleCustomersYesParkPerSquare;
		}
		
		
		
		for (int i = 0; i < satellitesOutliers; i++) {

			x = ThreadLocalRandom.current().nextInt(0, xMax+1);
			y = ThreadLocalRandom.current().nextInt(0, yMax+1);

			instance.addVertex(x, y, VertexType.SATELLITE, 0);
		}

		
		for (int i = 0; i < truckCustomersOutliers; i++) {


			x = ThreadLocalRandom.current().nextInt(0, xMax+1);
			y = ThreadLocalRandom.current().nextInt(0, yMax+1);

			int load = ThreadLocalRandom.current().nextInt(1, 50);
			
			instance.addVertex(x, y, VertexType.TRUCK_CUSTOMER, load);
		}

		
		for (int i = 0; i < vehicleCustomersNoParkOutliers; i++) {


			x = ThreadLocalRandom.current().nextInt(0, xMax+1);
			y = ThreadLocalRandom.current().nextInt(0, yMax+1);

			int load = ThreadLocalRandom.current().nextInt(1, 50);
			
			instance.addVertex(x, y, VertexType.VEHICLE_CUSTOMER_NO_PARK, load);
		}

		
		for (int i = 0; i < vehicleCustomersYesParkOutliers; i++) {


			x = ThreadLocalRandom.current().nextInt(0, xMax+1);
			y = ThreadLocalRandom.current().nextInt(0, yMax+1);

			int load = ThreadLocalRandom.current().nextInt(1, 50);
			
			instance.addVertex(x, y, VertexType.VEHICLE_CUSTOMER_YES_PARK, load);
		}
		
		
	}
	
	private static void generateRandom(Instance instance, int xMax, int yMax, int satellitesNum, int truckCustomersNum,
			int vehicleCustomersNum, int vehicleCustomersParkingFacilityNum) {

		int x = ThreadLocalRandom.current().nextInt(0, xMax + 1);
		int y = ThreadLocalRandom.current().nextInt(0, yMax + 1);
		
		instance.addVertex(x, y, VertexType.DEPOT, 0);
		instance.addVertex(x, y, VertexType.SATELLITE, 0);
		
		for (int i = 0; i < satellitesNum; i++) {

			x = ThreadLocalRandom.current().nextInt(0, xMax + 1);
			y = ThreadLocalRandom.current().nextInt(0, yMax + 1);

			instance.addVertex(x, y, VertexType.SATELLITE, 0);
		}

		
		for (int i = 0; i < truckCustomersNum; i++) {

			x = ThreadLocalRandom.current().nextInt(0, xMax + 1);
			y = ThreadLocalRandom.current().nextInt(0, yMax + 1);
			int load = ThreadLocalRandom.current().nextInt(1, 50);
			
			instance.addVertex(x, y, VertexType.TRUCK_CUSTOMER, load);
		}

		
		for (int i = 0; i < vehicleCustomersNum; i++) {

			x = ThreadLocalRandom.current().nextInt(0, xMax + 1);
			y = ThreadLocalRandom.current().nextInt(0, yMax + 1);
			int load = ThreadLocalRandom.current().nextInt(1, 50);
			
			instance.addVertex(x, y, VertexType.VEHICLE_CUSTOMER_NO_PARK, load);
		}

		
		for (int i = 0; i < vehicleCustomersParkingFacilityNum; i++) {

			x = ThreadLocalRandom.current().nextInt(0, xMax + 1);
			y = ThreadLocalRandom.current().nextInt(0, yMax + 1);
			int load = ThreadLocalRandom.current().nextInt(1, 50);
			
			instance.addVertex(x, y, VertexType.VEHICLE_CUSTOMER_YES_PARK, load);
		}

		
	}

	
}
