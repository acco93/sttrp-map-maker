package io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Stream;

import algorithms.Solution;
import model.Instance;
import model.Vertex;
import model.VertexType;

/**
 * 
 * Class with static methods that encapsulate the input/output logic.
 * 
 * @author acco
 *
 */
public class ioManager {

	public static void writeToFile(String path, Instance instance) {

		try {
			PrintWriter pw = new PrintWriter(new FileWriter(path));

			pw.printf("%d\t%d\t%d\t%d\n", instance.getTruckCustomers().size(), instance.getSatellites().size() + 1,
					instance.getVehicleCustomersNoPark().size(), instance.getVehicleCustomersYesPark().size());
			pw.printf("%d\t%d\n", instance.getTruckCapacity(), instance.getTrailerCapacity());
			pw.printf("%d\t%d\n", instance.getDepot().getX(), instance.getDepot().getY());
			for (Vertex satellite : instance.getSatellites()) {
				pw.printf("%d\t%d\n", satellite.getX(), satellite.getY());
			}
			for (Vertex customer : instance.getTruckCustomers()) {
				pw.printf("%d\t%d\t%d\n", customer.getX(), customer.getY(), customer.getLoad());
			}
			for (Vertex customer : instance.getVehicleCustomersNoPark()) {
				pw.printf("%d\t%d\t%d\n", customer.getX(), customer.getY(), customer.getLoad());
			}
			for (Vertex customer : instance.getVehicleCustomersYesPark()) {
				pw.printf("%d\t%d\t%d\n", customer.getX(), customer.getY(), customer.getLoad());
			}

			pw.close();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public static Instance readFromFile(String path, Instance instance) {

		if (parseStandardFormat(path, instance)) {
			return instance;
		}

		if (parseBartoliniFormat(path, instance)) {
			return instance;
		}

		if (parseChaoFormat(path, instance)) {
			return instance;
		}

		if (parseCVRTSPLibFormat(path, instance)) {
			return instance;
		}

		if (parseTuzunFormat(path, instance)) {
			return instance;
		}
		
		if(parseGoldenFormat(path, instance)) {
			return instance;
		}

		return null;

	}

	private static boolean parseCVRTSPLibFormat(String path, Instance instance) {
		File file = new File(path);

		try {
			Scanner sc = new Scanner(file);

			// name
			sc.nextLine();
			// comment
			sc.nextLine();
			// type
			sc.nextLine();

			// n
			String[] tokens = sc.nextLine().trim().split("\\s+");
			int customersNum = Integer.parseInt(tokens[2]);

			// edge type
			sc.nextLine();

			// capacity
			tokens = sc.nextLine().trim().split("\\s+");
			instance.setTruckCapacity(Integer.parseInt(tokens[2]));
			instance.setTrailerCapacity(999999);

			// node coord section
			sc.nextLine();

			ArrayList<Integer> x = new ArrayList<>(), y = new ArrayList<>(), q = new ArrayList<>();

			for (int i = 0; i < customersNum; i++) {
				tokens = sc.nextLine().trim().split("\\s+");
				x.add(Integer.parseInt(tokens[1]));
				y.add(Integer.parseInt(tokens[2]));
			}

			// demand section
			sc.nextLine();

			for (int i = 0; i < customersNum; i++) {
				tokens = sc.nextLine().trim().split("\\s+");
				q.add(Integer.parseInt(tokens[1]));
			}

			instance.addVertex(x.get(0), y.get(0), VertexType.DEPOT, 0);
			instance.addVertex(x.get(0), y.get(0), VertexType.SATELLITE, 0);
			for (int i = 1; i < customersNum; i++) {
				instance.addVertex(x.get(i), y.get(i), VertexType.TRUCK_CUSTOMER, q.get(i));
			}

			sc.close();

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	
	private static boolean parseGoldenFormat(String path, Instance instance) {
		File file = new File(path);

		try {
			Scanner sc = new Scanner(file);

			// name
			sc.nextLine();
			// comment
			sc.nextLine();
			// type
			sc.nextLine();

			// n
			String[] tokens = sc.nextLine().trim().split("\\s+");
			int customersNum = Integer.parseInt(tokens[1]);

			// capacity
			tokens = sc.nextLine().trim().split("\\s+");
			instance.setTruckCapacity(Integer.parseInt(tokens[1]));
			instance.setTrailerCapacity(999999);

			// vehicles
			sc.nextLine();

			sc.nextLine();
			sc.nextLine();
			sc.nextLine();
			
			// node coord section
			sc.nextLine();

			ArrayList<Integer> x = new ArrayList<>(), y = new ArrayList<>(), q = new ArrayList<>();

			for (int i = 0; i < customersNum-1; i++) {
				tokens = sc.nextLine().trim().split("\\s+");
				x.add((int) Float.parseFloat(tokens[1]));
				y.add((int) Float.parseFloat(tokens[2]));
			}

			// demand section
			sc.nextLine();

			for (int i = 0; i < customersNum-1; i++) {
				tokens = sc.nextLine().trim().split("\\s+");
				q.add(Integer.parseInt(tokens[1]));
			}

			for (int i = 0; i < customersNum-1; i++) {
				instance.addVertex(x.get(i), y.get(i), VertexType.TRUCK_CUSTOMER, q.get(i));
			}

			// depot section
			sc.nextLine();
			tokens = sc.nextLine().trim().split("\\s+");
			int xDepot = (int) (Float.parseFloat(tokens[0]));
			int yDepot = (int) (Float.parseFloat(tokens[1]));
			
			instance.addVertex(xDepot, yDepot, VertexType.DEPOT, 0);
			instance.addVertex(xDepot, yDepot, VertexType.SATELLITE, 0);
			
			sc.close();

		} catch (Exception e) {
			return false;
		}

		return true;
	}
	
	private static boolean parseBartoliniFormat(String path, Instance instance) {
		File file = new File(path);

		try {
			Scanner sc = new Scanner(file);

			String line = sc.nextLine().trim();

			// do whatever processing at the end of each line
			String[] tokens = line.split("\\s+");

			// tokens[0] = instance name
			// tokens[1] = number of trucks = 1
			instance.setTruckCapacity(Integer.parseInt(tokens[2]));
			// tokens[3] = number of trailers = 1
			instance.setTrailerCapacity(Integer.parseInt(tokens[4]));
			int customersNum = Integer.parseInt(tokens[5]);

			System.out.println(Arrays.deepToString(tokens));
			// depot
			line = sc.nextLine().trim();
			tokens = line.split("\\s+");
			System.out.println(Arrays.deepToString(tokens));

			// tokens[0] = vertex index
			instance.addVertex((int) Float.parseFloat(tokens[1]), (int) Float.parseFloat(tokens[2]), VertexType.DEPOT,
					0);

			for (int i = 0; i < customersNum; i++) {
				line = sc.nextLine().trim();
				tokens = line.split("\\s+");

				System.out.println(Arrays.deepToString(tokens));

				VertexType type;

				if (Integer.parseInt(tokens[4]) == 1) {
					// truck customer
					type = VertexType.TRUCK_CUSTOMER;
				} else {
					// vehicle customer with parking facility
					type = VertexType.VEHICLE_CUSTOMER_YES_PARK;
				}
				instance.addVertex((int) Float.parseFloat(tokens[1]), (int) Float.parseFloat(tokens[2]), type,
						(int) Float.parseFloat(tokens[3]));

			}

			sc.close();

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	private static boolean parseChaoFormat(String path, Instance instance) {
		File file = new File(path);

		try {
			Scanner sc = new Scanner(file);

			String line = sc.nextLine().trim();

			// do whatever processing at the end of each line
			String[] tokens = line.split("\\s+");

			instance.setTruckCapacity((int) Float.parseFloat(tokens[0]));
			instance.setTrailerCapacity((int) Float.parseFloat(tokens[1]));
			int customersNum = Integer.parseInt(tokens[2]);

			System.out.println(Arrays.deepToString(tokens));
			// depot
			line = sc.nextLine().trim();
			tokens = line.split("\\s+");
			System.out.println(Arrays.deepToString(tokens));

			// tokens[0] = vertex index
			instance.addVertex((int) Float.parseFloat(tokens[1]), (int) Float.parseFloat(tokens[2]), VertexType.DEPOT,
					0);
			instance.addVertex((int) Float.parseFloat(tokens[1]), (int) Float.parseFloat(tokens[2]),
					VertexType.SATELLITE, 0);

			for (int i = 0; i < customersNum; i++) {
				line = sc.nextLine().trim();
				tokens = line.split("\\s+");

				System.out.println(Arrays.deepToString(tokens));

				VertexType type;

				if (Integer.parseInt(tokens[4]) == 1) {
					// truck customer
					type = VertexType.TRUCK_CUSTOMER;
				} else {
					// vehicle customer with parking facility
					type = VertexType.VEHICLE_CUSTOMER_YES_PARK;
				}
				instance.addVertex((int) Float.parseFloat(tokens[1]), (int) Float.parseFloat(tokens[2]), type,
						(int) Float.parseFloat(tokens[3]));

			}

			sc.close();

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	private static boolean parseTuzunFormat(String path, Instance instance) {

		File file = new File(path);

		try {

			Scanner sc = new Scanner(file);

			String line = sc.nextLine().trim();
			String[] tokens = line.split("\\s+");
			int truckCustomers = Integer.parseInt(tokens[0]);

			line = sc.nextLine().trim();
			tokens = line.split("\\s+");
			int satellites = Integer.parseInt(tokens[0]);

			line = sc.nextLine();

			float[] xSatellites = new float[satellites];
			float[] ySatellites = new float[satellites];

			for (int k = 0; k < satellites; k++) {

				line = sc.nextLine().trim();
				tokens = line.split("\\s+");

				xSatellites[k] = Float.parseFloat(tokens[0]);
				ySatellites[k] = Float.parseFloat(tokens[1]);

			}

			float[] xCustomers = new float[truckCustomers];
			float[] yCustomers = new float[truckCustomers];
			int[] qCustomers = new int[truckCustomers];

			for (int i = 0; i < truckCustomers; i++) {

				line = sc.nextLine().trim();
				tokens = line.split("\\s+");

				xCustomers[i] = Float.parseFloat(tokens[0]);
				yCustomers[i] = Float.parseFloat(tokens[1]);

			}

			line = sc.nextLine().trim();

			line = sc.nextLine().trim();
			tokens = line.split("\\s+");
			instance.setTruckCapacity((int) Float.parseFloat(tokens[0]));
			instance.setTrailerCapacity(9999999);

			line = sc.nextLine().trim();

			for (int k = 0; k < satellites; k++) {
				line = sc.nextLine().trim();
			}

			line = sc.nextLine().trim();

			for (int i = 0; i < truckCustomers; i++) {

				line = sc.nextLine().trim();
				tokens = line.split("\\s+");

				qCustomers[i] = Integer.parseInt(tokens[0]);
				

			}

			
			instance.addVertex(0, 0, VertexType.DEPOT, 0);
			for(int k = 0; k < satellites; k++) {
				instance.addVertex((int)xSatellites[k], (int)ySatellites[k], VertexType.SATELLITE, 0);
			}
			
			for(int i = 0; i < truckCustomers; i++) {
				instance.addVertex((int)xCustomers[i], (int)yCustomers[i], VertexType.TRUCK_CUSTOMER, 0);
			}
			
			sc.close();

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	private static boolean parseStandardFormat(String path, Instance instance) {
		File file = new File(path);

		try {
			Scanner sc = new Scanner(file);

			String line = sc.nextLine().trim();

			// do whatever processing at the end of each line
			String[] tokens = line.split("\\s");

			int truckCustomers = Integer.parseInt(tokens[0]);
			int satellites = Integer.parseInt(tokens[1]) - 1;
			int vehicleCustomers = 0;
			int vehicleCustomersParkingFacility = 0;

			try {

				vehicleCustomers = Integer.parseInt(tokens[2]);
				vehicleCustomersParkingFacility = Integer.parseInt(tokens[3]);

			} catch (Exception e) {
				vehicleCustomers = 0;
				vehicleCustomersParkingFacility = 0;

			}

			instance.setTruckCapacity(sc.nextInt());
			instance.setTrailerCapacity(sc.nextInt());

			instance.addVertex(sc.nextInt(), sc.nextInt(), VertexType.DEPOT, 0);

			for (int i = 0; i < satellites; i++) {
				instance.addVertex(sc.nextInt(), sc.nextInt(), VertexType.SATELLITE, 0);
			}

			for (int i = 0; i < truckCustomers; i++) {
				instance.addVertex(sc.nextInt(), sc.nextInt(), VertexType.TRUCK_CUSTOMER, sc.nextInt());
			}

			for (int i = 0; i < vehicleCustomers; i++) {
				instance.addVertex(sc.nextInt(), sc.nextInt(), VertexType.VEHICLE_CUSTOMER_NO_PARK, sc.nextInt());
			}

			for (int i = 0; i < vehicleCustomersParkingFacility; i++) {
				instance.addVertex(sc.nextInt(), sc.nextInt(), VertexType.VEHICLE_CUSTOMER_YES_PARK, sc.nextInt());
			}

			sc.close();

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	private static boolean parseMultiDepotCVRPFormat(String path, Instance instance) {

		File file = new File(path);

		try {
			Scanner sc = new Scanner(file);

			String line = sc.nextLine().trim();

			// do whatever processing at the end of each line
			String[] tokens = line.split("\\s+");

			int type = Integer.parseInt(tokens[0]);
			if (type != 2) {
				sc.close();
				throw new Error("Not a MDCVRP instance");
			}

			int vehicles = Integer.parseInt(tokens[1]);
			int truckCustomers = Integer.parseInt(tokens[2]);
			int satellites = Integer.parseInt(tokens[3]);
			int vehicleCustomers = 0;
			int vehicleCustomersParkingFacility = 0;

			tokens = sc.nextLine().trim().split("\\s+");
			instance.setTruckCapacity(Integer.parseInt(tokens[1]));
			instance.setTrailerCapacity(9999999);

			for (int i = 1; i < satellites; i++) {
				sc.nextLine().trim().split("\\s");
			}

			// add a dummy depot
			instance.addVertex(-111110, -111110, VertexType.DEPOT, 0);

			for (int i = 0; i < truckCustomers; i++) {
				tokens = sc.nextLine().trim().split("\\s+");
				instance.addVertex(Integer.parseInt(tokens[1]), Integer.parseInt(tokens[2]), VertexType.TRUCK_CUSTOMER,
						Integer.parseInt(tokens[4]));
			}

			for (int i = 0; i < satellites; i++) {
				tokens = sc.nextLine().trim().split("\\s+");
				instance.addVertex(Integer.parseInt(tokens[1]), Integer.parseInt(tokens[2]), VertexType.SATELLITE, 0);
			}

			sc.close();

		} catch (Exception e) {
			return false;
		}

		return true;

	}

	public static Solution readSolution(InputStream stream, Instance instance) {

		Solution solution = null;
		try {

			int matrixSize = instance.getVerticesNum();

			/*
			 * Map instances list into a well defined array position
			 */
			int[] x = new int[matrixSize];
			int[] y = new int[matrixSize];
			int[] q = new int[matrixSize];
			VertexType[] vtype = new VertexType[matrixSize];
			int idx = 0;
			/*
			 * Truck customers
			 */
			for (Vertex vertex : instance.getTruckCustomers()) {
				x[idx] = vertex.getX();
				y[idx] = vertex.getY();
				q[idx] = vertex.getLoad();
				vtype[idx] = vertex.getType();
				idx++;
			}

			/*
			 * Vehicle customers
			 */
			for (Vertex vertex : instance.getVehicleCustomersNoPark()) {
				x[idx] = vertex.getX();
				y[idx] = vertex.getY();
				q[idx] = vertex.getLoad();
				vtype[idx] = vertex.getType();
				idx++;
			}

			/*
			 * Vehicle customers parking facility
			 */
			for (Vertex vertex : instance.getVehicleCustomersYesPark()) {
				x[idx] = vertex.getX();
				y[idx] = vertex.getY();
				q[idx] = vertex.getLoad();
				vtype[idx] = vertex.getType();
				idx++;
			}

			/*
			 * Satellites
			 */
			for (Vertex vertex : instance.getSatellites()) {
				x[idx] = vertex.getX();
				y[idx] = vertex.getY();
				q[idx] = vertex.getLoad();
				vtype[idx] = vertex.getType();
				idx++;
			}

			/*
			 * Depot
			 */
			x[idx] = instance.getDepot().getX();
			y[idx] = instance.getDepot().getY();
			q[idx] = instance.getDepot().getLoad();
			vtype[idx] = instance.getDepot().getType();
			/*
			 * Compute matrix distance
			 */

			float[][] c = new float[matrixSize][matrixSize];
			for (int i = 0; i < matrixSize; i++) {

				int xi = x[i];
				int yi = y[i];

				for (int j = 0; j < matrixSize; j++) {

					int xj = x[j];
					int yj = y[j];

					c[i][j] = (float) Math.pow((float) Math.pow((float) (xi - xj), 2) + Math.pow((float) (yi - yj), 2),
							(float) 0.5);

				}

			}

			Scanner sc = new Scanner(stream);

			float cost = 0;

			Set<List<Vertex>> routes = new HashSet<>();
			/* Sub tours */
			while (sc.hasNextLine()) {

				String line = sc.nextLine();
				String[] vertices = line.split("[ ,]");

				int tourLoad = 0;
				ArrayList<Vertex> subTour = new ArrayList<Vertex>();
				for (String vertex : vertices) {
					int index = Integer.parseInt(vertex);
					subTour.add(new Vertex(x[index], y[index], vtype[index], q[index]));
					tourLoad += q[index];
					System.out.print(index + " ");
				}
				System.out.println(": " + tourLoad);
				routes.add(subTour);

				for (int i = 0; i < vertices.length - 1; i++) {
					int a = Integer.parseInt(vertices[i]);
					int b = Integer.parseInt(vertices[i + 1]);
					cost += c[a][b];
				}

			}

			solution = new Solution(routes, cost);

			sc.close();

		} catch (Exception e) {

			return null;

		}

		return solution;

	}

	public static void writeToFileAsCvrp(String path, Instance instance) {

		try {
			PrintWriter pw = new PrintWriter(new FileWriter(path));

			pw.printf("NAME : name\n");
			pw.printf("COMMENT : (comment)\n");
			pw.printf("TYPE : CVRP\n");
			pw.printf("DIMENSION : %d\n", instance.getTruckCustomers().size());
			pw.printf("EDGE_WEIGHT_TYPE : EUC_2D\n");
			pw.printf("CAPACITY : %d\n", instance.getTruckCapacity());
			pw.printf("NODE_COORD_SECTION\n");
			pw.printf("1 %d %d\n", instance.getDepot().getX(), instance.getDepot().getY());
			for (int i = 0; i < instance.getTruckCustomers().size(); i++) {
				pw.printf("%d %d %d\n", i + 2, instance.getTruckCustomers().get(i).getX(),
						instance.getTruckCustomers().get(i).getY());
			}
			pw.printf("DEMAND_SECTION\n");
			pw.printf("1 0\n");
			for (int i = 0; i < instance.getTruckCustomers().size(); i++) {
				pw.printf("%d %d\n", i + 2, instance.getTruckCustomers().get(i).getLoad());
			}
			pw.printf("DEPOT_SECTION\n");
			pw.printf(" 1\n");
			pw.printf(" -1\n");
			pw.printf("EOF\n");
			pw.close();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public static void convertInstancesFromDirectory(File path) {
		try (Stream<Path> paths = Files.walk(Paths.get(path.getAbsolutePath()))) {
			paths.filter(Files::isRegularFile).forEach((file) -> {
				Instance instance = new Instance();
				instance = ioManager.readFromFile(file.toAbsolutePath().toString(), instance);
				if (instance != null) {
					instance.setTrailerCapacity(10000);
					ioManager.writeToFile(file.toAbsolutePath().toString() + ".converted", instance);
				}
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static int num = 0;

	public static void convertChaoInstancesToXSTTRPOnesFromDirectory(File path) {

		try (Stream<Path> paths = Files.walk(Paths.get(path.getAbsolutePath()))) {
			paths.filter(Files::isRegularFile).forEach((file) -> {
				Instance instance = new Instance();
				instance = ioManager.readFromFile(file.toAbsolutePath().toString(), instance);
				if (instance != null) {

					ArrayList<Integer> satellitesRange = new ArrayList<>();

					int nodes = instance.getTruckCustomers().size() + instance.getVehicleCustomersYesPark().size() + 1;

					satellitesRange.add(5);

					if (nodes >= 100) {
						satellitesRange.add(10);

						if (nodes >= 200) {
							satellitesRange.add(20);
						}

					}

					float vehicleCustomersNoParkFactor1 = 1.0f / 5.0f;
					float vehicleCustomersNoParkFactor2 = 4.0f / 5.0f;

					ArrayList<Float> vehicleCustomersNoParkFactors = new ArrayList<>();
					vehicleCustomersNoParkFactors.add(vehicleCustomersNoParkFactor1);
					vehicleCustomersNoParkFactors.add(vehicleCustomersNoParkFactor2);

					for (int s : satellitesRange) {

						for (float v : vehicleCustomersNoParkFactors) {

							Instance newInstance = Instance.deepCopy(instance);

							List<Vertex> customers = new ArrayList<>();

							customers.addAll(newInstance.getTruckCustomers());
							customers.addAll(newInstance.getVehicleCustomersYesPark());

							Collections.shuffle(customers);

							for (int k = 0; k < s; k++) {
								Vertex vertex = customers.get(k);
								newInstance.removeVertex(vertex);
								newInstance.addVertex(vertex.getX(), vertex.getY(), VertexType.SATELLITE, 0);
							}

							List<Vertex> vehicleCustomers = new ArrayList<>();
							vehicleCustomers.addAll(newInstance.getVehicleCustomersYesPark());

							Collections.shuffle(vehicleCustomers);

							int vehicleCustomersNoParkNum = (int) Math.floor(vehicleCustomers.size() * v);

							for (int i = 0; i < vehicleCustomersNoParkNum; i++) {
								Vertex vertex = vehicleCustomers.get(i);
								newInstance.removeVertex(vertex);
								newInstance.addVertex(vertex.getX(), vertex.getY(), VertexType.VEHICLE_CUSTOMER_NO_PARK,
										vertex.getLoad());
							}

							// ioManager.writeToFile(file.toAbsolutePath().toString()+
							// "."+s+"."+v+".converted", newInstance);
							ioManager.writeToFile(file.toAbsolutePath().toString() + ".xsttrp" + num, newInstance);

							num++;
						}

					}
				}

			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void convertCurrentCVRPInstance(Instance instance) {
		
		ArrayList<Integer> satellitesRange = new ArrayList<>();
		satellitesRange.add(20);
		satellitesRange.add(40);
		satellitesRange.add(60);
		satellitesRange.add(80);
		satellitesRange.add(100);
		
		ArrayList<Float> truckToVehicleRatios = new ArrayList<>();
		truckToVehicleRatios.add(0.25f);
		truckToVehicleRatios.add(0.50f);
		truckToVehicleRatios.add(0.75f);
		
		
		ArrayList<Float> vehicleCustomersWithParkToWithoutParkRatios = new ArrayList<>();
		vehicleCustomersWithParkToWithoutParkRatios.add(0.2f);
		vehicleCustomersWithParkToWithoutParkRatios.add(0.8f);
		
		int lll = 0;
		
		for(Integer s : satellitesRange) {
			
			for(Float t : truckToVehicleRatios) {
				
				for(Float v : truckToVehicleRatios) {
					
					Instance copy = Instance.deepCopy(instance);
					
					Instance xsttrpInstance = new Instance();
					
					xsttrpInstance.setTruckCapacity(copy.getTruckCapacity());
					xsttrpInstance.setTrailerCapacity(99999999);
					
					xsttrpInstance.addVertex(copy.getDepot().getX(), copy.getDepot().getY(), VertexType.DEPOT, 0);
					
					ArrayList<Vertex> customers = new ArrayList<>();
					customers.addAll(copy.getTruckCustomers());
					
					Collections.shuffle(customers);
					
					int index = 0;
					
					for(; index < s; index++) {
						Vertex vertex = customers.get(index);
						xsttrpInstance.addVertex(vertex.getX(), vertex.getY(), VertexType.SATELLITE, 0);
					}
					
					
					int remainingNum = customers.size() - s;
					
					int truckCustomers = (int) (remainingNum * t);
					int vehicleCustomers = remainingNum - truckCustomers;
					int vehicleCustomersNoPark = (int) (vehicleCustomers * v);
					int vehicleCustomersYesPark = vehicleCustomers - vehicleCustomersNoPark;
					
					for(int i = 0; i < truckCustomers; i++, index++) {
						Vertex vertex = customers.get(index);
						xsttrpInstance.addVertex(vertex.getX(), vertex.getY(), VertexType.TRUCK_CUSTOMER, vertex.getLoad());						
					}

					for(int i = 0; i < vehicleCustomersNoPark; i++, index++) {
						Vertex vertex = customers.get(index);
						xsttrpInstance.addVertex(vertex.getX(), vertex.getY(), VertexType.VEHICLE_CUSTOMER_NO_PARK, vertex.getLoad());						
					}
					
					for(int i = 0; i < vehicleCustomersYesPark; i++, index++) {
						Vertex vertex = customers.get(index);
						xsttrpInstance.addVertex(vertex.getX(), vertex.getY(), VertexType.VEHICLE_CUSTOMER_YES_PARK, vertex.getLoad());						
					}					
					
					int leftOvers = customers.size() - s - truckCustomers - vehicleCustomersNoPark - vehicleCustomersYesPark;
					
					for(int i = 0; i < leftOvers; i++, index++) {
						Vertex vertex = customers.get(index);
						xsttrpInstance.addVertex(vertex.getX(), vertex.getY(), VertexType.TRUCK_CUSTOMER, vertex.getLoad());						
					}
					
					ioManager.writeToFile("/home/acco/CLionProjects/xsttrp/instances/xsttrp-xl/xl"+lll, xsttrpInstance);
					
					lll++;
				}
				
			}
			
		}
		
	}
	
	
	public static Instance readFromFileMDCVRP(String path, Instance instance) {

		if (parseMultiDepotCVRPFormat(path, instance)) {
			return instance;
		}
		return null;

	}

}
