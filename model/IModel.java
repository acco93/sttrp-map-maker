package model;

/**
 * Model interface.
 * 
 * @author acco
 *
 */
public interface IModel {

	/**
	 * Returns the current instance.
	 * 
	 * @return the instance
	 */
	Instance getInstance();

	/**
	 * Create a new empty instance.
	 */
	void resetInstance();
}
