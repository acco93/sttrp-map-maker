package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import resources.R;

/**
 * 
 * Problem instance representation.
 * 
 * @author acco
 *
 */
public class Instance {

	/*
	 * At a given x,y position there might be different vertices, this map maintains
	 * the mapping position to list of vertices
	 */
	private Map<Position, List<Vertex>> mapping;

	private int truckCapacity;
	private int trailerCapacity;

	private Vertex depot;
	private List<Vertex> truckCustomers;
	private List<Vertex> vehicleCustomers;
	private List<Vertex> satellites;
	private List<Vertex> vehicleCustomersParkingFacility;

	public Instance() {

		this.reset();

	}

	/**
	 * Add a vertex to the instance if nothing exists in the same coordinates.
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 * @param type
	 *            vertex type
	 * @param load
	 *            load value
	 */
	public boolean addVertex(int x, int y, VertexType type, int load) {

		Vertex vertex = new Vertex(x, y, type, load);

		Position pos = new Position(x, y);
		List<Vertex> list = mapping.get(pos);

		if (list == null) {
			/*
			 * It was an empty position
			 */
			list = new ArrayList<>();
			mapping.put(pos, list);
		}

		list.add(vertex);

		/*
		 * Add the vertex to its list
		 */
		switch (type) {
		case DEPOT:
			this.depot = vertex;
			return true;
		case SATELLITE:
			return this.satellites.add(vertex);

		case TRUCK_CUSTOMER:
			return this.truckCustomers.add(vertex);

		case VEHICLE_CUSTOMER_NO_PARK:
			return this.vehicleCustomers.add(vertex);
		case VEHICLE_CUSTOMER_YES_PARK:
			return this.vehicleCustomersParkingFacility.add(vertex);
		default:
			return false;
		}

	}

	/**
	 * Remove a vertex in the given position if it exists.
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 */
	public void removeVertex(int x, int y) {

		Position pos = new Position(x, y);
		List<Vertex> list = this.mapping.get(pos);

		if (list == null) {
			return;
		}

		/*
		 * Always remove the first vertex
		 */
		Vertex vertex = list.get(0);

		if (list.size() == 1) {
			this.mapping.remove(pos);
		}
		list.remove(vertex);

		if (vertex.equals(depot)) {
			depot = null;
		}

		satellites.remove(vertex);
		truckCustomers.remove(vertex);
		vehicleCustomers.remove(vertex);
		vehicleCustomersParkingFacility.remove(vertex);

	}

	/**
	 * Returns the vertex in the given position, if exists.
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 * @return the vertex if found, null otherwise
	 */
	public List<Vertex> getVertex(int x, int y) {

		return this.mapping.get(new Position(x, y));

	}

	public int getTruckCapacity() {
		return truckCapacity;
	}

	public void setTruckCapacity(int truckCapacity) {
		this.truckCapacity = truckCapacity;
	}

	public int getTrailerCapacity() {
		return trailerCapacity;
	}

	public void setTrailerCapacity(int trailerCapacity) {
		this.trailerCapacity = trailerCapacity;
	}

	public Vertex getDepot() {
		return depot;
	}

	public List<Vertex> getTruckCustomers() {
		return truckCustomers;
	}

	public List<Vertex> getVehicleCustomersNoPark() {
		return vehicleCustomers;
	}

	public List<Vertex> getSatellites() {
		return satellites;
	}
	
	public List<Vertex> getVehicleCustomersYesPark() {
		return vehicleCustomersParkingFacility;
	}

	public void reset() {
		this.mapping = new HashMap<>();

		this.truckCapacity = R.TRUCK_CAPACITY_DEFAULT_VALUE;
		this.trailerCapacity = R.TRAILER_CAPACITY_DEFAULT_VALUE;

		this.depot = null;
		this.truckCustomers = new ArrayList<Vertex>();
		this.vehicleCustomers = new ArrayList<Vertex>();

		this.vehicleCustomersParkingFacility = new ArrayList<Vertex>();

		this.satellites = new ArrayList<Vertex>();
		
	}

	public int getVerticesNum() {

		return 1 + this.satellites.size() + this.truckCustomers.size()+this.vehicleCustomers.size() + this.vehicleCustomersParkingFacility.size();
	}

	public void removeVertex(Vertex vertex) {
		
		Position pos = new Position(vertex.getX(), vertex.getY());
		List<Vertex> list = this.mapping.get(pos);

		if (list == null) {
			return;
		}

		boolean removed = list.remove(vertex);
		
		boolean res = false;
		
		switch(vertex.getType()) {
		case DEPOT:
			depot = null;
			break;
		case SATELLITE:
			res = satellites.remove(vertex);
			break;
		case TRUCK_CUSTOMER:
			res = truckCustomers.remove(vertex);
			break;
		case VEHICLE_CUSTOMER_NO_PARK:
			res = vehicleCustomers.remove(vertex);
			break;
		case VEHICLE_CUSTOMER_YES_PARK:
			res = vehicleCustomersParkingFacility.remove(vertex);
			break;
		default:
			break;
		
		}
		
	}

	public static Instance deepCopy(Instance instance) {

		Instance result = new Instance();
		result.setTruckCapacity(instance.getTruckCapacity());
		result.setTrailerCapacity(instance.getTrailerCapacity());

		result.addVertex(instance.getDepot().getX(), instance.getDepot().getY(), instance.getDepot().getType(),
				instance.getDepot().getLoad());

		for (Vertex vertex : instance.getTruckCustomers()) {
			result.addVertex(vertex.getX(), vertex.getY(), vertex.getType(), vertex.getLoad());
		}

		for (Vertex vertex : instance.getSatellites()) {
			result.addVertex(vertex.getX(), vertex.getY(), vertex.getType(), vertex.getLoad());
		}

		for (Vertex vertex : instance.getVehicleCustomersNoPark()) {
			result.addVertex(vertex.getX(), vertex.getY(), vertex.getType(), vertex.getLoad());
		}

		for (Vertex vertex : instance.getVehicleCustomersYesPark()) {
			result.addVertex(vertex.getX(), vertex.getY(), vertex.getType(), vertex.getLoad());
		}
		
		return result;
	}


}
