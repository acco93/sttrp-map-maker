package model;

/**
 * Model implementation.
 * 
 * @author acco
 *
 */
public class Model implements IModel {

	private Instance instance;

	public Model() {
		this.instance = new Instance();
	}

	@Override
	public Instance getInstance() {
		return this.instance;
	}


	@Override
	public void resetInstance() {
		this.instance.reset();
		
	}

}
