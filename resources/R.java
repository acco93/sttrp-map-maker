package resources;

import java.awt.Color;

public class R {

	public static final int WINDOW_HEIGHT = 600;
	public static final int WINDOW_WIDTH = 800;
	public static final int TRUCK_CAPACITY_DEFAULT_VALUE = 300;
	public static final int TRAILER_CAPACITY_DEFAULT_VALUE = 9999;
	public static final Color VERY_LIGHT_GRAY = new Color(240,240,240);
	public static final Color VERY_LIGHT_BLUE = new Color(210, 230, 244);
	public static final Color BOOTSTRAP_RED = new Color(217,83,79);
	public static final Color BOOTSTRAP_GREEN = new Color(92,184,92);


}
