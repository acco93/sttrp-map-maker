package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import resources.R;

public class EditPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EditPanel() {
		
		this.setBackground(Color.WHITE);
		
		JPanel mainPanel = new JPanel();
		mainPanel.setOpaque(false);
		mainPanel.setLayout(new GridLayout(0,1));
		

		JTextField truckCapacityField = new JTextField(String.valueOf(R.TRUCK_CAPACITY_DEFAULT_VALUE));
		mainPanel.add(this.configPanel("Truck capacity", truckCapacityField));
		
		JTextField trailerCapacityField = new JTextField(String.valueOf(R.TRAILER_CAPACITY_DEFAULT_VALUE));
		mainPanel.add(this.configPanel("Trailer capacity", trailerCapacityField));
		
		this.setLayout(new BorderLayout());
		this.add(mainPanel, BorderLayout.NORTH);
		
	}
	
	public JPanel configPanel(String title, JComponent component) {

		JPanel panel = new JPanel();
		panel.setOpaque(false);
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS) );
		
		JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		topPanel.setOpaque(false);
		topPanel.add(new JLabel(title));
		topPanel.add(component);

		panel.add(topPanel);
		
		
		
		return panel;
	}
	
}
