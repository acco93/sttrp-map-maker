package view;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class EmptyProjectPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmptyProjectPanel() {
		
		this.setLayout(new BorderLayout());

		JLabel label = new JLabel("Empty project", SwingConstants.CENTER);
		label.setFont(new Font(label.getFont().getFontName(), Font.BOLD, 40));	
	
		this.add(label, BorderLayout.CENTER);
		
	}
	
}
