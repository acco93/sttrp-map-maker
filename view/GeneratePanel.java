package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controller.IController;
import resources.R;

public class GeneratePanel extends JPanel implements WindowListener {

	private static final long serialVersionUID = 1L;

	public GeneratePanel(View view, IController controller) {

		this.setBackground(Color.WHITE);

		JPanel mainPanel = new JPanel();
		mainPanel.setOpaque(false);
		mainPanel.setLayout(new GridLayout(0, 1));

		JLabel xLabel = new JLabel("MAX X COORDINATE", SwingConstants.CENTER);
		xLabel.setBackground(Color.WHITE);
		xLabel.setOpaque(true);
		mainPanel.add(xLabel);
		JTextField xField = new JTextField("100");
		xField.setHorizontalAlignment(JTextField.CENTER);
		xField.setBorder(new EmptyBorder(10, 10, 10, 10));
		xField.setBackground(R.VERY_LIGHT_BLUE);
		mainPanel.add(xField);

		JLabel yLabel = new JLabel("MAX Y COORDINATE", SwingConstants.CENTER);
		yLabel.setBackground(Color.WHITE);
		yLabel.setOpaque(true);
		mainPanel.add(yLabel);
		JTextField yField = new JTextField("100");
		yField.setHorizontalAlignment(JTextField.CENTER);
		yField.setBorder(new EmptyBorder(10, 10, 10, 10));
		yField.setBackground(R.VERY_LIGHT_BLUE);
		mainPanel.add(yField);

		JLabel satellitesLabel = new JLabel("SATELLITES NUM.", SwingConstants.CENTER);
		satellitesLabel.setBackground(Color.WHITE);
		satellitesLabel.setOpaque(true);
		mainPanel.add(satellitesLabel);
		JTextField satellitesField = new JTextField("5");
		satellitesField.setHorizontalAlignment(JTextField.CENTER);
		satellitesField.setBorder(new EmptyBorder(10, 10, 10, 10));
		satellitesField.setBackground(R.VERY_LIGHT_BLUE);
		mainPanel.add(satellitesField);

		JLabel truckCustomersLabel = new JLabel("TRUCK CUSTOMERS NUM.", SwingConstants.CENTER);
		truckCustomersLabel.setBackground(Color.WHITE);
		truckCustomersLabel.setOpaque(true);
		mainPanel.add(truckCustomersLabel);
		JTextField truckCustomersField = new JTextField("40");
		truckCustomersField.setHorizontalAlignment(JTextField.CENTER);
		truckCustomersField.setBorder(new EmptyBorder(10, 10, 10, 10));
		truckCustomersField.setBackground(R.VERY_LIGHT_BLUE);
		mainPanel.add(truckCustomersField);

		JLabel vehicleCustomersLabel = new JLabel("VEHICLE CUSTOMERS NUM.", SwingConstants.CENTER);
		vehicleCustomersLabel.setBackground(Color.WHITE);
		vehicleCustomersLabel.setOpaque(true);
		mainPanel.add(vehicleCustomersLabel);
		JTextField vehicleCustomersField = new JTextField("0");
		vehicleCustomersField.setEnabled(true);
		vehicleCustomersField.setHorizontalAlignment(JTextField.CENTER);
		vehicleCustomersField.setBorder(new EmptyBorder(10, 10, 10, 10));
		vehicleCustomersField.setBackground(R.VERY_LIGHT_BLUE);
		mainPanel.add(vehicleCustomersField);
		
		JLabel vehicleCustomersParkingFacilityLabel = new JLabel("VEHICLE CUSTOMERS PARKING FACILITY NUM.", SwingConstants.CENTER);
		vehicleCustomersParkingFacilityLabel.setBackground(Color.WHITE);
		vehicleCustomersParkingFacilityLabel.setOpaque(true);
		mainPanel.add(vehicleCustomersParkingFacilityLabel);
		JTextField vehicleCustomersParkingFacilityField = new JTextField("0");
		vehicleCustomersParkingFacilityField.setEnabled(true);
		vehicleCustomersParkingFacilityField.setHorizontalAlignment(JTextField.CENTER);
		vehicleCustomersParkingFacilityField.setBorder(new EmptyBorder(10, 10, 10, 10));
		vehicleCustomersParkingFacilityField.setBackground(R.VERY_LIGHT_BLUE);
		mainPanel.add(vehicleCustomersParkingFacilityField);


		JLabel instanceLabel = new JLabel("INSTANCE TYPE", SwingConstants.CENTER);
		instanceLabel.setBackground(Color.WHITE);
		instanceLabel.setOpaque(true);
		mainPanel.add(instanceLabel);
		ButtonGroup buttonGroup = new ButtonGroup();

		JToggleButton random = new JToggleButton("Random");
		random.setSelected(true);
		JToggleButton clustered = new JToggleButton("Clustered");
		buttonGroup.add(random);
		buttonGroup.add(clustered);
		JPanel buttonsPanel = new JPanel(new FlowLayout());
		buttonsPanel.setOpaque(false);
		buttonsPanel.add(random);
		buttonsPanel.add(clustered);
		mainPanel.add(buttonsPanel);

		JPanel commandPanel = new JPanel(new FlowLayout());
		commandPanel.setOpaque(false);
		JButton generate = new JButton("Generate");
		generate.addActionListener((e) -> {
			try {
				int xMax = Integer.parseInt(xField.getText());
				int yMax = Integer.parseInt(yField.getText());
				int satellitesNum = Integer.parseInt(satellitesField.getText());
				int truckCustomersNum = Integer.parseInt(truckCustomersField.getText());
				int vehicleCustomersNum = Integer.parseInt(vehicleCustomersField.getText());
				int vehicleCustomersParkingFacilityNum = Integer.parseInt(vehicleCustomersParkingFacilityField.getText());
				boolean type = random.isSelected();
				controller.generate(xMax, yMax, satellitesNum, truckCustomersNum, vehicleCustomersNum, vehicleCustomersParkingFacilityNum, type);
			} catch (NumberFormatException nfe) {

			}

		});
		JButton close = new JButton("Close");
		close.addActionListener((e) -> {
			view.hideGeneratePanel();
		});
		commandPanel.add(generate);
		commandPanel.add(close);

		this.setLayout(new BorderLayout());
		this.add(mainPanel, BorderLayout.NORTH);
		this.add(commandPanel, BorderLayout.SOUTH);

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {
		this.setVisible(false);
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

}
