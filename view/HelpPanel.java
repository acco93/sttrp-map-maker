package view;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import resources.R;

public class HelpPanel extends JFrame {

	private static final long serialVersionUID = 1L;
	private StyledDocument doc;
	
	public HelpPanel(View view) {
		this.setSize(600, 400);
		this.setLocationRelativeTo(view);

		this.setTitle("Help");
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		
		JPanel mainPanel = new JPanel();
		mainPanel.setBackground(Color.WHITE);
		
		mainPanel.setLayout(new BorderLayout());
		
		JLabel titleLabel = new JLabel("Simple Single Truck and Trailer Map Editor", SwingUtilities.CENTER);
		titleLabel.setOpaque(false);
		mainPanel.add(titleLabel, BorderLayout.NORTH);


		JTextPane textArea = new JTextPane();
		textArea.setEditable(false);

		doc = textArea.getStyledDocument();

		Style titleStyle = textArea.addStyle("Title style", null);
		StyleConstants.setBackground(titleStyle, new Color(247, 200, 207));
		StyleConstants.setBold(titleStyle, true);
		//StyleConstants.setBackground(titleStyle, new Color(161, 57, 71));

		Style linkStyle = textArea.addStyle("Link style", null);
		StyleConstants.setBackground(linkStyle, new Color(158, 196, 188));
		StyleConstants.setItalic(linkStyle, true);
		//StyleConstants.setForeground(linkStyle, new Color(209, 228, 224));

		Style paperStyle = textArea.addStyle("Paper style", null);
		StyleConstants.setBackground(paperStyle, new Color(255, 226, 207));
		StyleConstants.setItalic(paperStyle, true);
		
		this.append("..:: SHORTCUTS AND COMMANDS ::..", titleStyle);
		this.append("Move map: drag mouse with wheel pressed or arrows", null);
		this.append("Zoom in and out: rotate mouse wheel or + and -", null);
		this.append("Select satellite: s or toolbar button", null);
		this.append("Select truck customer: t or toolbar button", null);
		this.append("Select depot: d or toolbar button",null);
		this.append("Delete a vertex: right click",null);
		
		this.newLine();
		
		this.append("..:: GENERAL DESCRIPTION ::..", titleStyle);
		this.append("STTRP Map Maker is a standalone old style Java application developed to ease STTRP instances creation. It provides the following features:",null);
		this.append("- import/export and visual editing of a defined set of STTRP file format instances",null);
		this.append("- automatic instance generation where vertices are randomly distributed",null);
		this.append("- a small set of processing features such as checking for instance feasibility and computing rough upper bound",null);
		
		this.append("The application is in its early alpha and it currently supports only STTRPSD instances where there is a single vehicle composed of a truck and a trailer, a single depot and the vertices might be truck customers or satellite depots.", null);
		this.newLine();
		
		this.append("..:: FILE STRUCTURE ::..", titleStyle);
		this.append("The application works with files formatted as follows:", null);
		this.append("The first line indicates the size of the problem: number of customers (n) and number of depots (p+1);", null);
		this.append("The second line has the capacities of the truck and the trailer;", null);
		this.append("p+1 lines with the x and y coordinates of the depots (the first line is for the main depot and the next correspond to the satellite depots);", null);
		this.append("n lines with the x and y coordinates of the customers and their demands.", null);
		this.newLine();
		
		this.newLine();
		this.append("Public repository available at:",null);
		this.append("https://bitbucket.org/acco93/sttrp-map-maker",linkStyle);
		this.newLine();
		
		
		

			
		JScrollPane scroll = new JScrollPane(textArea);
		scroll.setBackground(Color.WHITE);
		scroll.setOpaque(false);
		scroll.setBorder(new EmptyBorder(20, 20, 20, 20));

		mainPanel.add(scroll, BorderLayout.CENTER);
		
		this.add(mainPanel);
		this.setVisible(false);	
	}
	
	private void newLine() {
		this.append("", null);

	}

	private void append(String text, Style style) {
		try {
			doc.insertString(doc.getLength(), text + "\n", style);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}
	
	public void showHelp() {
		this.setVisible(true);
	}
	
	public void hideHelp() {
		this.setVisible(false);
	}
	
}
