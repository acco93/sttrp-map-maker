package view;

import algorithms.Solution;

/**
 * View interface.
 * 
 * @author acco
 *
 */
public interface IView {

	void setMousePosition(int x, int y);

	void refresh();

	void resetPosition();

	void setError(String string);

	void lock();
	
	void unlock();

	void setSolution(Solution solution);

	void setInfo(String string);

	boolean isLocked();

	void setOptimalSolution(Solution solution);

	void clearSolution(boolean isOptimal);

}
