package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controller.IController;
import resources.R;

public class InputSolution extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean isOptimal;
	private JLabel xLabel;

	public InputSolution(IController controller, View view) {
	
		
		
		this.setBackground(Color.WHITE);

		JPanel mainPanel = new JPanel();
		mainPanel.setOpaque(false);
		mainPanel.setLayout(new BorderLayout());
		
		xLabel = new JLabel("INPUT SOLUTION", SwingConstants.CENTER);
		xLabel.setBackground(Color.WHITE);
		xLabel.setOpaque(true);
		xLabel.setBorder(new EmptyBorder(10, 10, 10, 10));
		mainPanel.add(xLabel, BorderLayout.NORTH);
		

		JTextPane textArea = new JTextPane();


		textArea.setBackground(R.VERY_LIGHT_BLUE);


		textArea.setBorder(new EmptyBorder(10, 10, 10, 10));
		
		JScrollPane scroll = new JScrollPane(textArea);
		scroll.setBorder(new EmptyBorder(0,0,0,0));
		scroll.setPreferredSize(new Dimension(400, 600));
		
		mainPanel.add(scroll, BorderLayout.CENTER);
		
		
		JPanel commandPanel = new JPanel(new FlowLayout());
		commandPanel.setOpaque(false);
		JButton close = new JButton("Close");
		close.addActionListener((e) -> {
			view.hideInputSolutionPanel();
		});
	
		
		JButton apply = new JButton("Apply");
		apply.addActionListener((e) -> {
			String text = textArea.getText();
			controller.importSolutionFromStdin(text, isOptimal);
		});
		
		JButton clear = new JButton("Clear");
		apply.addActionListener((e) -> {
			controller.clearSolution(isOptimal);
		});
		
		commandPanel.add(apply);
		commandPanel.add(clear);
		commandPanel.add(close);
		
		
		
		this.setLayout(new BorderLayout());
		this.add(mainPanel, BorderLayout.CENTER);
		this.add(commandPanel, BorderLayout.SOUTH);

		
		
	}

	public void setOptimal(boolean isOptimal) {
		if(isOptimal) {
			xLabel.setText("Write the optimal solution:");
		} else {
			xLabel.setText("Write the solution:");			
		}
		this.isOptimal = isOptimal;
	}
	
	
	public void showPanel() {
		this.setVisible(true);
	}

}
