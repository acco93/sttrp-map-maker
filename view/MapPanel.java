package view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import algorithms.Solution;
import controller.IController;
import model.Instance;
import model.Vertex;
import model.VertexType;
import resources.R;

public class MapPanel extends JPanel implements KeyListener {

	private static final long serialVersionUID = 1L;
	private IController controller;
	private int span;
	private Point currentPoint;
	private VertexType currentVertexType;
	private int toLeft = 0;
	private int toTop = 0;
	private ToolBar toolbar;
	private boolean showNumbers = false;

	private String text = "";
	private Point textPoint = null;
	private Solution solution = null;
	private IView view;
	private Solution optimalSolution = null;
	private boolean upperBoundVisible;
	private boolean exactVisible;
	private boolean moveUsingMouse;

	private boolean hqRendering = false;
	private boolean showGrid = true;
	private boolean showMouse = false;
	private boolean hideDepotArcs = false;

	public MapPanel(IController controller, IView view) {

		this.view = view;
		this.controller = controller;

		this.span = 16;
		this.currentPoint = new Point(-1, -1);
		this.currentVertexType = VertexType.DEPOT;

		this.addKeyListener(this);

		this.setBackground(Color.WHITE);
		this.setFocusable(true);

		MouseAdapter ma = new MouseAdapter() {

			private Point startDragPoint;

			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				int notches = e.getWheelRotation();
				if (notches < 0) {
					zoomIn();
				} else {
					zoomOut();
				}
				repaint();
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				if (SwingUtilities.isMiddleMouseButton(e) || moveUsingMouse) {

					int x = (int) Math.floor(e.getPoint().x / span) - toLeft;
					int y = (int) Math.floor(e.getPoint().y / span) - toTop;

					int xOffset = x - startDragPoint.x;
					int yOffset = y - startDragPoint.y;

					toLeft += xOffset;
					toTop += yOffset;

					repaint();
				}
			}

			@Override
			public void mouseMoved(MouseEvent e) {

				textPoint = null;

				int x = (int) Math.floor(e.getPoint().x / span);
				int y = (int) Math.floor(e.getPoint().y / span);

				// now that x and y are in the grid compute the right point
				currentPoint = new Point(x * span, y * span);

				view.setMousePosition(x - toLeft, y - toTop);

				List<Vertex> found = controller.getInstance().getVertex(x - toLeft, y - toTop);

				if (found != null) {
					textPoint = new Point(e.getX() - 10, e.getY() - 20);

					text = found.toString();

				}

				repaint();

			}

			@Override
			public void mousePressed(MouseEvent e) {

				if (view.isLocked()) {
					return;
				}

				int x = (int) Math.floor(e.getPoint().x / span) - toLeft;
				int y = (int) Math.floor(e.getPoint().y / span) - toTop;

				if (SwingUtilities.isLeftMouseButton(e) && !moveUsingMouse) {

					int load = 0;

					if (currentVertexType != VertexType.DEPOT && currentVertexType != VertexType.SATELLITE) {
						/*
						 * String loadString = JOptionPane.showInputDialog("Load = "); try { load =
						 * Integer.parseInt(loadString); } catch (NumberFormatException exc) { return; }
						 */
						load = ThreadLocalRandom.current().nextInt(1, 30);
					}
					controller.getInstance().addVertex(x, y, currentVertexType, load);
					view.refresh();

				} else if (SwingUtilities.isRightMouseButton(e)) {

					controller.getInstance().removeVertex(x, y);
					view.refresh();

				} else if (SwingUtilities.isMiddleMouseButton(e) || moveUsingMouse) {

					startDragPoint = new Point(x, y);

				}

				grabFocus();
				repaint();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				startDragPoint = null;
			}

		};

		this.addMouseListener(ma);
		this.addMouseMotionListener(ma);
		this.addMouseWheelListener(ma);

	
		
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(new BasicStroke(1));

		if (hqRendering) {
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		}

		if (showGrid) {
			g.setColor(R.VERY_LIGHT_GRAY);
			for (int i = span / 2; i < this.getWidth(); i += span) {
				g.drawLine(i, 0, i, this.getHeight());
			}
			for (int j = span / 2; j < this.getHeight(); j += span) {
				g.drawLine(0, j, this.getWidth(), j);
			}
			g.setColor(Color.red);
			g.drawLine(this.xValueOf(0) + span / 2, this.yValueOf(0) + span / 2, this.xValueOf(0) + span / 2,
					this.getHeight());
			g.drawLine(this.xValueOf(0) + span / 2, this.yValueOf(0) + span / 2, this.getWidth(),
					this.yValueOf(0) + span / 2);
		}

		if (showMouse) {
			g.setColor(Color.YELLOW);
			g.fillOval(currentPoint.x, currentPoint.y, span, span);
		}

	
		if (solution != null && upperBoundVisible) {


			for (List<Vertex> route : solution.getRoutes()) {

				// creates a copy of the Graphics instance
				Graphics2D g2d = (Graphics2D) g.create();

				// set the stroke of the copy, not the original
				Stroke dashed = new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[] { span }, 0);

				
				if (route.get(0).equals(controller.getInstance().getDepot())) {
					g2d.setColor(Color.RED);
					g2d.setStroke(new BasicStroke(2));
				} else {
					g2d.setStroke(dashed);
					g2d.setColor(Color.BLUE);
				}

				int begin = 0;
				int end = route.size() - 1;
				if (hideDepotArcs) {
					begin++;
					end--;
				}

				for (int i = begin; i < end; i++) {
					int x1 = this.xValueOf(route.get(i).getX());
					int y1 = this.yValueOf(route.get(i).getY());
					int x2 = this.xValueOf(route.get(i + 1).getX());
					int y2 = this.yValueOf(route.get(i + 1).getY());
					g2d.drawLine(x1 + span / 2, y1 + span / 2, x2 + span / 2, y2 + span / 2);
				}

		
				
			}


		}

		Instance instance = controller.getInstance();
		g.setColor(Color.BLACK);

		int vertexId = 0;

		for (Vertex vertex : instance.getTruckCustomers()) {
			int x = this.xValueOf(vertex.getX());
			int y = this.yValueOf(vertex.getY());

			if (showNumbers) {
				g.drawString(Integer.toString(vertexId), x, y - span);
			}

			g.setColor(Color.WHITE);
			g.fillOval(x, y, span, span);
			g.setColor(Color.BLACK);
			g.drawOval(x, y, span, span);

			vertexId++;
		}

		for (Vertex vertex : instance.getVehicleCustomersNoPark()) {
			int x = this.xValueOf(vertex.getX());
			int y = this.yValueOf(vertex.getY());

			if (showNumbers) {
				g.drawString(Integer.toString(vertexId), x, y - span);
			}

			g.setColor(Color.BLACK);
			g.fillOval(x, y, span, span);

			vertexId++;
		}

		for (Vertex vertex : instance.getVehicleCustomersYesPark()) {
			int x = this.xValueOf(vertex.getX());
			int y = this.yValueOf(vertex.getY());

			if (showNumbers) {
				g.drawString(Integer.toString(vertexId), x, y - span);
			}

			g.setColor(Color.BLACK);
			g.fillPolygon(new int[] { x, x + span / 2, x + span }, new int[] { y + span, y - span / 4, y + span }, 3);

			vertexId++;
		}

		for (Vertex vertex : instance.getSatellites()) {

			int x = this.xValueOf(vertex.getX());
			int y = this.yValueOf(vertex.getY());

			if (showNumbers) {
				g.drawString(Integer.toString(vertexId), x, y - span);
			}

			g.setColor(Color.WHITE);
			g.fillPolygon(new int[] { x, x + span / 2, x + span }, new int[] { y + span, y - span / 4, y + span }, 3);
			g.setColor(Color.BLACK);
			g.drawPolygon(new int[] { x, x + span / 2, x + span }, new int[] { y + span, y - span / 4, y + span }, 3);

			vertexId++;
		}

		if (instance.getDepot() != null) {
			int x = this.xValueOf(instance.getDepot().getX());
			int y = this.yValueOf(instance.getDepot().getY());
			if (showNumbers) {
				g.drawString(Integer.toString(vertexId), x, y - span);
			}
			g.setColor(Color.WHITE);
			g.fillRect(x, y, span, span);
			g.setColor(Color.BLACK);
			g.drawRect(x, y, span, span);

			vertexId++;
		}

		for (Vertex vertex : instance.getVehicleCustomersNoPark()) {
			int x = this.xValueOf(vertex.getX());
			int y = this.yValueOf(vertex.getY());
			g.fillOval(x, y, span, span);
		}

		if (textPoint != null) {
			g.drawString(text, textPoint.x, textPoint.y);

		}

	}

	@Override
	public Dimension getPreferredSize() {
		return this.getSize();
	}

	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		switch (code) {
		case KeyEvent.VK_LEFT:
			this.toLeft++;
			break;
		case KeyEvent.VK_RIGHT:
			this.toLeft--;
			break;
		case KeyEvent.VK_UP:
			this.toTop++;
			break;
		case KeyEvent.VK_DOWN:
			this.toTop--;
			break;
		case KeyEvent.VK_MINUS:
			this.zoomOut();
			break;
		case KeyEvent.VK_PLUS:
			this.zoomIn();
			break;
		case KeyEvent.VK_SHIFT:

			break;
		case KeyEvent.VK_H:
			upperBoundVisible = !upperBoundVisible;
			break;
		case KeyEvent.VK_X:
			exactVisible = !exactVisible;
			break;
		case KeyEvent.VK_S:

			Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
			try {
				String content = (String) c.getContents(null).getTransferData(DataFlavor.stringFlavor);
				controller.importSolutionFromStdin(content, false);
			} catch (UnsupportedFlavorException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
			break;
		case KeyEvent.VK_N:
			this.showNumbers = !this.showNumbers;
			break;
		}

		repaint();
	}

	public void keyReleased(KeyEvent e) {

	}



	public void setToolbar(ToolBar toolbar) {
		this.toolbar = toolbar;
	}

	public void setCurrentVertexType(VertexType type) {
		this.currentVertexType = type;
		this.toolbar.setSelected(type);
		this.moveUsingMouse = false;
	}

	private void zoomIn() {
		if (this.span == 40) {
			return;
		}
		this.span++;
	}

	private void zoomOut() {
		if (this.span == 1) {
			return;
		}
		this.span--;
	}

	private int xValueOf(int x) {
		return x * span + toLeft * span;
	}

	private int yValueOf(int y) {
		return y * span + toTop * span;
	}

	public void refresh() {
		this.solution = null;
		this.repaint();
	}

	public void resetPosition() {
		this.toLeft = 0;
		this.toTop = 0;
		repaint();
	}

	public void setSolution(Solution solution) {
		this.solution = solution;
		this.upperBoundVisible = true;
		repaint();
	}

	public void setOptimalSolution(Solution solution) {
		this.optimalSolution = solution;
		this.exactVisible = true;
		repaint();
	}

	public void moveUsingMouse() {
		this.moveUsingMouse = true;

	}

	public void toggleGrid() {
		this.showGrid = !this.showGrid;
		repaint();
	}

	public void toggleMouse() {
		this.showMouse = !this.showMouse;
		repaint();
	}

	public void toggleHQRendering() {
		this.hqRendering = !this.hqRendering;
		repaint();
	}

	public void clearSolution(boolean isOptimal) {
		if (isOptimal) {
			this.optimalSolution = null;
		} else {
			this.solution = null;
		}
		repaint();
	}

	public void toggleDepotArcs() {
		this.hideDepotArcs = !this.hideDepotArcs;

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
