package view;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import controller.IController;
import io.ioManager;

/**
 * 
 * JMenuBar extension.
 * 
 * @author acco
 *
 */
public class MenuBar extends JMenuBar {

	private static final long serialVersionUID = 1L;
	private JMenuItem newItem;
	private JMenuItem exportItem;
	private JMenuItem importItem;
	private JMenuItem exitItem;
	private JMenuItem exportAsCvrp;
	private JCheckBoxMenuItem showGrid;
	private JCheckBoxMenuItem hqRendering;
	private JCheckBoxMenuItem showMouse;
	private JMenuItem convertItem;
	private JCheckBoxMenuItem hideDepotArcs;
	private JMenuItem importMDCVRPItem;
	private JMenuItem overwriteItem;
	private JMenuItem convertChaoItem;

	public MenuBar(IController controller, View view) {
				
		/*
		 * File menu definition
		 */
		JMenu fileMenu = new JMenu("File");
		newItem = new JMenuItem("New instance");
		exportItem = new JMenuItem("Save instance");
		overwriteItem = new JMenuItem("Overwrite!");
		exportAsCvrp = new JMenuItem("Save as CVRP instance");
		importItem = new JMenuItem("Load instance");
		importMDCVRPItem = new JMenuItem("Load MDCVRP instance");
		convertItem = new JMenuItem("Convert instances");
		convertChaoItem = new JMenuItem("Convert Chao -> XSTTRP");
		exitItem = new JMenuItem("Exit");

		this.add(fileMenu);
		fileMenu.add(newItem);
		fileMenu.addSeparator();
		fileMenu.add(exportItem);
		fileMenu.add(overwriteItem);
		fileMenu.add(exportAsCvrp);
		fileMenu.add(importItem);
		fileMenu.add(importMDCVRPItem);
		fileMenu.addSeparator();
		fileMenu.add(convertItem);
		fileMenu.add(convertChaoItem);
		fileMenu.addSeparator();
		fileMenu.add(exitItem);

		newItem.addActionListener((e)->{
			controller.resetInstance();
		});
		
		exportItem.addActionListener((ActionEvent e) -> {

			if (!controller.isFeasible()) {
				JOptionPane.showMessageDialog(null, "The instance is not feasible.", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}

			JFileChooser fileChooser = new JFileChooser();
			if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				controller.store(file.getPath());
			}

		});
		
		overwriteItem.addActionListener(e -> {
			controller.overwrite();
		});
		
		
		exportAsCvrp.addActionListener((ActionEvent e) -> {

			if (!controller.isCvrpFeasible()) {
				JOptionPane.showMessageDialog(null, "The instance is not feasible.", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}

			JFileChooser fileChooser = new JFileChooser();
			if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				controller.storeAsCvrp(file.getPath());
			}

		});

		importItem.addActionListener((ActionEvent e) -> {

			JFileChooser fileChooser = new JFileChooser();
			if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				controller.load(file.getPath());
			}

		});
		
		importMDCVRPItem.addActionListener((e) -> {

			JFileChooser fileChooser = new JFileChooser();
			if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				controller.loadMDCVRP(file.getPath());
			}
			
		});

		exitItem.addActionListener((ActionEvent e) -> {
			controller.exit();
		});
		
		convertItem.addActionListener((e)->{
			JFileChooser fileChooser = new JFileChooser();
			if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				File path = fileChooser.getCurrentDirectory();
				controller.convert(path);
			}
		});
		
		convertChaoItem.addActionListener(e->{
			ioManager.convertCurrentCVRPInstance(controller.getInstance());
			/*JFileChooser fileChooser = new JFileChooser();
			if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				File path = fileChooser.getCurrentDirectory();
				controller.convertChao(path);
			}*/
		});

		JMenu processMenu = new JMenu("Process");
		JMenuItem feasibilityButton = new JMenuItem("Check feasibility");
		feasibilityButton.addActionListener((e)->{
			if (!controller.isFeasible()) {
				JOptionPane.showMessageDialog(null, "The instance is not feasible.", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			} else {
				JOptionPane.showMessageDialog(null, "The instance is feasible.", "Info",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		
		
		JMenu loadSolutionButton = new JMenu("Load solution");
		JMenuItem loadSolutionFromFile = new JMenuItem("From file");
		loadSolutionFromFile.addActionListener((e)->{
			JFileChooser fileChooser = new JFileChooser();
			if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				controller.importSolutionFromFile(file.getPath(), false);
			}
		});
		loadSolutionButton.add(loadSolutionFromFile);
		
		JMenuItem loadSolutionFromClipboard = new JMenuItem("From clipboard");
		loadSolutionFromClipboard.addActionListener(e -> {
			
			Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
			try {
				String content = (String) c.getContents(null).getTransferData(DataFlavor.stringFlavor);
				controller.importSolutionFromStdin(content, false);
			} catch (UnsupportedFlavorException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
			
			JOptionPane.showMessageDialog(null, "Next time just press S while the map is focused.", "Tip",
					JOptionPane.INFORMATION_MESSAGE);
			
		});
		loadSolutionButton.add(loadSolutionFromClipboard);
		
		JMenuItem loadSolutionFromStdin = new JMenuItem("Type it");
		loadSolutionFromStdin.addActionListener((e)->{
			view.showInputSolutionPanel(false);
		});
		loadSolutionButton.add(loadSolutionFromStdin);
		
		
		JMenuItem generateItem = new JMenuItem("Generate instance");
		generateItem.addActionListener((e)->{
			view.showGeneratePanel();
		});
		
		this.add(processMenu);
		processMenu.add(feasibilityButton);
		processMenu.add(loadSolutionButton);
		processMenu.addSeparator();
		processMenu.add(generateItem);
		
		
		JMenu viewMenu = new JMenu("View");
		showGrid = new JCheckBoxMenuItem("Show grid");
		showGrid.setSelected(true);
		hqRendering = new JCheckBoxMenuItem("HQ rendering");
		showMouse = new JCheckBoxMenuItem("Show mouse shadow");
		hideDepotArcs = new JCheckBoxMenuItem("Hide depot arcs");

		this.add(viewMenu);
		viewMenu.add(showGrid);
		viewMenu.add(showMouse);
		viewMenu.addSeparator();
		viewMenu.add(hqRendering);
		viewMenu.addSeparator();
		viewMenu.add(hideDepotArcs);
		
		showGrid.addActionListener((e)->{ view.toggleGrid();});
		showMouse.addActionListener((e)->{ view.toggleMouse();});
		hqRendering.addActionListener((e)->{ view.toggleHQRendering();});
		hideDepotArcs.addActionListener((e)->{view.toggleDepotArcs();});
		
		HelpPanel helpPanel = new HelpPanel(view);
		
		JMenu helpMenu = new JMenu("Help");
		this.add(helpMenu);
		JMenuItem howItWorksButton = new JMenuItem("How it works?");
		howItWorksButton.addActionListener(e->{
			helpPanel.showHelp();
		});
		helpMenu.add(howItWorksButton);
		
		

	}

}
