package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import controller.IController;
import model.Instance;

public class StatusBar extends JPanel {

	private static final long serialVersionUID = 1L;
	private JLabel mousePosition;
	private JLabel info;
	private JLabel statistics;
	private IController controller;

	public StatusBar(IController controller, IView view) {

		this.controller = controller;

		this.setLayout(new BorderLayout());

		this.setBackground(Color.WHITE);
		this.mousePosition = new JLabel(" (0, 0)");
		this.mousePosition.setToolTipText("Click to reset the position");
		this.mousePosition.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				view.resetPosition();
			}

		});
		this.add(this.mousePosition, BorderLayout.WEST);

		this.info = new JLabel("",SwingConstants.CENTER);
		this.add(info, BorderLayout.CENTER);

		this.statistics = new JLabel("Hello!");
		this.add(statistics, BorderLayout.EAST);
	}

	public void setMousePosition(int x, int y) {
		this.mousePosition.setText(" (" + x + ", " + y + ")");
	}

	public void setInfo(String message) {
		SwingUtilities.invokeLater(()->{
			this.info.setForeground(Color.BLUE);
			this.info.setText(message);			
		});

	}

	public void updateStatistics() {
		Instance instance = controller.getInstance();

		int truckCustomersLoad = instance.getTruckCustomers().stream().map(vertex -> vertex.getLoad()).reduce(0,
				(x, y) -> x + y);

		int vehicleCustomersLoad = instance.getVehicleCustomersNoPark().stream().map(vertex -> vertex.getLoad()).reduce(0,
				(x, y) -> x + y);

		this.statistics.setText(instance.getTruckCustomers().size() + "-" + instance.getVehicleCustomersNoPark().size() + "-"
				+ instance.getVehicleCustomersYesPark().size() + "-"
				+ instance.getSatellites().size() + "-" + truckCustomersLoad + "/" + instance.getTruckCapacity() + "-"
				+ vehicleCustomersLoad + "/" + instance.getTrailerCapacity());
	}

	public void refresh() {
		this.updateStatistics();
		this.info.setText("");
	}

	public void setError(String string) {
		SwingUtilities.invokeLater(()->{
			this.info.setForeground(Color.RED);
			this.info.setText(string);
		});

	}

}
