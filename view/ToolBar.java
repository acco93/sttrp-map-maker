package view;

import java.awt.Graphics;
import java.awt.event.ActionEvent;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import controller.IController;
import model.VertexType;

public class ToolBar extends JToolBar {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JToggleButton depot;
	private JToggleButton satellite;
	private JToggleButton truckCustomer;
	private JToggleButton vehicleCustomer;
	private JToggleButton move;
	private JToggleButton vehicleCustomerParkingFacility;
	private JToggleButton lock;


	public ToolBar(MapPanel map, IController controller) {

		this.setFloatable(false);

		ButtonGroup buttonGroup = new ButtonGroup();		
		move = new JToggleButton(new ImageIcon("resources/move.png"));
		move.setToolTipText("Move the environment using the mouse right click");
		move.addActionListener((ActionEvent e) -> {
			map.moveUsingMouse();
		});
		buttonGroup.add(move);
		
		depot = new JToggleButton(new ImageIcon("resources/depot.png"));
		depot.setToolTipText("Depot");
		depot.addActionListener((ActionEvent e) -> {
			map.setCurrentVertexType(VertexType.DEPOT);
		});
		buttonGroup.add(depot);

		satellite = new JToggleButton(new ImageIcon("resources/satellite.png"));
		depot.setToolTipText("Satellite");
		satellite.addActionListener((ActionEvent e) -> {
			map.setCurrentVertexType(VertexType.SATELLITE);
		});
		buttonGroup.add(satellite);

		truckCustomer = new JToggleButton(new ImageIcon("resources/truck_customer.png"));
		depot.setToolTipText("Truck customer");
		truckCustomer.addActionListener((ActionEvent e) -> {
			map.setCurrentVertexType(VertexType.TRUCK_CUSTOMER);
		});
		buttonGroup.add(truckCustomer);

		vehicleCustomer = new JToggleButton(new ImageIcon("resources/vehicle_customer.png"));
		depot.setToolTipText("Vehicle customer without parking facility");
		vehicleCustomer.addActionListener((ActionEvent e) -> {
			map.setCurrentVertexType(VertexType.VEHICLE_CUSTOMER_NO_PARK);
		});
		vehicleCustomer.setEnabled(true);
		buttonGroup.add(vehicleCustomer);

		
		vehicleCustomerParkingFacility = new JToggleButton(new ImageIcon("resources/vehicle_customer_parking_location.png"));
		depot.setToolTipText("Vehicle customer with parking facility");
		vehicleCustomerParkingFacility.addActionListener((ActionEvent e) -> {
			map.setCurrentVertexType(VertexType.VEHICLE_CUSTOMER_YES_PARK);
		});
		vehicleCustomerParkingFacility.setEnabled(true);
		buttonGroup.add(vehicleCustomerParkingFacility);
		
		depot.setSelected(true);

		this.add(move);
		this.add(depot);
		this.add(satellite);
		this.add(truckCustomer);
		this.add(vehicleCustomer);
		this.add(vehicleCustomerParkingFacility);
		
		this.add(Box.createHorizontalGlue());

		JButton truckCapacity = new JButton("Set Truck Capacity");
		truckCapacity.addActionListener((ActionEvent e) -> {
			String tcString = JOptionPane.showInputDialog(map, "Truck Capacity = ",
					controller.getTruckCapacity());
			try {
				int tc = Integer.parseInt(tcString);
				controller.setTruckCapacity(tc);

			} catch (NumberFormatException exc) {
				return;
			}
		});

		JButton trailerCapacity = new JButton("Set Trailer Capacity");
		trailerCapacity.addActionListener((ActionEvent e) -> {
			String tcString = JOptionPane.showInputDialog(map, "Trailer Capacity = ",
					controller.getTrailerCapacity());
			try {
				int tc = Integer.parseInt(tcString);
				controller.setTrailerCapacity(tc);

			} catch (NumberFormatException exc) {
				return;
			}
		});

		this.add(truckCapacity);
		this.add(trailerCapacity);

	}

	public void setSelected(VertexType type) {

		switch (type) {
		case DEPOT:
			this.depot.setSelected(true);
			break;
		case SATELLITE:
			this.satellite.setSelected(true);
			break;
		case TRUCK_CUSTOMER:
			this.truckCustomer.setSelected(true);
			break;
		case VEHICLE_CUSTOMER_NO_PARK:
			this.vehicleCustomer.setSelected(true);
			break;
		default:
			break;

		}

	}

}

class FigureButton extends JButton {

	private static final long serialVersionUID = 1L;
	private Painter painter;

	public FigureButton(Painter painter) {
		this.painter = painter;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		painter.paint(g);
	}
}

interface Painter {
	public void paint(Graphics g);
}