package view;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import algorithms.Solution;
import controller.Controller;
import resources.R;

/**
 * View implementation.
 * 
 * @author acco
 *
 */
public class View extends JFrame implements IView {

	private static final long serialVersionUID = 1L;
	private Controller controller;
	private StatusBar statusBar;
	private MapPanel map;
	private ViewLock lock;
	private GeneratePanel generateView;
	private JScrollPane scrollGenerateView;
	private InputSolution inputSolution;
	private EmptyProjectPanel emptyProjectPanel;

	public View(Controller controller) {

		this.controller = controller;

		this.lock = new ViewLock();

		this.setTitle("Map Maker");
		this.setSize(new Dimension(R.WINDOW_WIDTH, R.WINDOW_HEIGHT));
		this.setLocationRelativeTo(null);
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				controller.exit();
			}
		});

		this.setJMenuBar(new MenuBar(controller, this));

		statusBar = new StatusBar(controller, this);
		this.add(statusBar, BorderLayout.SOUTH);

	
		
		map = new MapPanel(this.controller, this);
		this.add(map, BorderLayout.CENTER);

		ToolBar toolbar = new ToolBar(map, this.controller);
		this.add(toolbar, BorderLayout.NORTH);

		map.setToolbar(toolbar);


		this.generateView = new GeneratePanel(this, controller);
		scrollGenerateView = new JScrollPane(generateView);
		scrollGenerateView.setVisible(false);
		scrollGenerateView.setBorder(BorderFactory.createMatteBorder(0, 5, 0, 0, R.VERY_LIGHT_BLUE));

		this.add(scrollGenerateView, BorderLayout.EAST);
		
		inputSolution = new InputSolution(controller, this);
		inputSolution.setVisible(false);
		inputSolution.setBorder(BorderFactory.createMatteBorder(0, 0, 0,5, R.VERY_LIGHT_BLUE));
		this.add(inputSolution, BorderLayout.WEST);
		
		this.setVisible(true);

		map.requestFocusInWindow();

	}

	@Override
	public void setMousePosition(int x, int y) {
		this.statusBar.setMousePosition(x, y);
	}

	@Override
	public void refresh() {
		this.map.refresh();
		this.statusBar.refresh();
	}

	@Override
	public void resetPosition() {
		this.map.resetPosition();
	}

	@Override
	public void setError(String string) {
		this.statusBar.setError(string);
	}

	@Override
	public void lock() {
		this.lock.lock();

	}

	@Override
	public void unlock() {
		this.lock.unlock();
	}

	@Override
	public void setSolution(Solution solution) {
		this.map.setSolution(solution);
		
	}

	@Override
	public void setInfo(String string) {
		this.statusBar.setInfo(string);
		
	}

	void showGeneratePanel() {
		scrollGenerateView.setVisible(true);
		this.revalidate();
	}
	
	void hideGeneratePanel() {
		scrollGenerateView.setVisible(false);
		this.revalidate();
	}

	@Override
	public boolean isLocked() {
		return this.lock.isLocked();
	}

	@Override
	public void setOptimalSolution(Solution solution) {
		this.map.setOptimalSolution(solution);
		
	}

	public void hideInputSolutionPanel() {
		inputSolution.setVisible(false);
		this.revalidate();
	}

	public void showInputSolutionPanel(boolean optimal) {
		inputSolution.setOptimal(optimal);
		inputSolution.setVisible(true);
		this.revalidate();
	}

	public void toggleGrid() {
		this.map.toggleGrid();
	}

	public void toggleMouse() {
		this.map.toggleMouse();
	}

	public void toggleHQRendering() {
		this.map.toggleHQRendering();
	}

	@Override
	public void clearSolution(boolean isOptimal) {
		this.map.clearSolution(isOptimal);
	}

	public void toggleDepotArcs() {
		this.map.toggleDepotArcs();
		
	}

	

}
