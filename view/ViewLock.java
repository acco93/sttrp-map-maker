package view;

public class ViewLock {

	private volatile boolean isLocked;

	public ViewLock() {
		this.isLocked = false;
	}

	public synchronized void lock() {
		this.isLocked = true;
	}

	public synchronized void unlock() {
		this.isLocked = false;
	}

	public synchronized boolean isLocked() {
		return this.isLocked;
	}

}
